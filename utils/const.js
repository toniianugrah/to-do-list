export const completedOptions = [
  { label: "In progress", value: "on progress", variant: "inprogress" },
  { label: "Blocked", value: "blocked", variant: "blocked" },
  { label: "Done", value: "done", variant: "done" },
];
