export const imageToUrl = (image) => {
  return `data:${image.contentType};base64,${Buffer.from(
    image.data.data
  ).toString("base64")}`;
};
