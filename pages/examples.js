import { Button } from "@/components/Button";
import { Checkbox } from "@/components/Checkbox";
import { Input } from "@/components/Input";
import { Layout } from "@/components/Layout";
import { CircularLoading } from "@/components/Loading";
import { Select } from "@/components/Select";
import { StatusLabel } from "@/components/StatusLabel";
import { Tag } from "@/components/Tag";
import { EmptyTodos, TodoCard } from "@/components/Todo";
import { useState } from "react";
import { completedOptions } from "utils/const";

export default function Home() {
  // --- Checkbox example ---
  const [checkedItems, setCheckedItems] = useState([false, false]);
  const isAllChecked = checkedItems.every(Boolean);
  const isIndeterminate = checkedItems.some(Boolean) && !isAllChecked;
  // ---

  const [selected, setSelected] = useState(completedOptions[0]);

  return (
    <Layout title="Examples">
      <h1 className="mt-4 text-3xl font-bold underline">Examples</h1>
      <h2 className="mt-8 text-xl font-bold">Pages</h2>
      <div className="flex mt-2 space-x-2">
        <Button variant="secondary" href="/signin">
          Sign in
        </Button>
        <Button variant="secondary" href="/register">
          Register
        </Button>
        <Button variant="secondary" href="/todos">
          Dashboard
        </Button>
        <Button variant="secondary" href="/todos/1">
          Todo Detail
        </Button>
      </div>
      <h2 className="mt-8 text-xl font-bold">Button</h2>
      <div className="mt-2">
        <div className="flex mb-2 space-x-2">
          <Button>Primary</Button>
          <Button variant="secondary">Secondary</Button>
          <Button variant="danger">Danger</Button>
        </div>
        <div className="flex space-x-2">
          <Button variant="text">Text</Button>
          <Button variant="text-danger">Text Danger</Button>
        </div>
      </div>
      <h2 className="mt-8 text-xl font-bold">Loading Indicator</h2>
      <div className="mt-4">
        <CircularLoading />
      </div>
      <h2 className="mt-8 text-xl font-bold">Text Input</h2>
      <div className="mt-2">
        <Input />
      </div>
      <h2 className="mt-8 text-xl font-bold">Select</h2>
      <div className="mt-2">
        <Select
          value={selected}
          options={completedOptions}
          onChange={setSelected}
        />
      </div>
      <h2 className="mt-8 text-xl font-bold">Status Label</h2>
      <div className="flex mt-2 space-x-2">
        <StatusLabel variant="inprogress">In progress</StatusLabel>
        <StatusLabel variant="blocked">Blocked</StatusLabel>
        <StatusLabel variant="done">Done </StatusLabel>
      </div>
      <h2 className="mt-8 text-xl font-bold">Tag</h2>
      <div className="flex mt-2 space-x-2">
        <Tag>#tag-1</Tag>
        <Tag>#tag-2</Tag>
        <Tag>#tag-3</Tag>
      </div>
      <h2 className="mt-8 text-xl font-bold">Checkbox</h2>
      <div className="mt-4">
        <div className="flex items-center">
          <Checkbox
            id="c12"
            checked={
              isAllChecked ? true : isIndeterminate ? "indeterminate" : false
            }
            onCheckedChange={(value) => {
              setCheckedItems([value, value]);
            }}
          />
          <label htmlFor="c12" className="ml-2 text-sm text-gray-900">
            Pekerjaan rumah
          </label>
        </div>
        <div className="flex flex-col pl-8 mt-4 space-y-4">
          <div className="flex items-center">
            <Checkbox
              checked={checkedItems[0]}
              onCheckedChange={(value) =>
                setCheckedItems([value, checkedItems[1]])
              }
              id="c1"
            />
            <label htmlFor="c1" className="ml-2 text-sm text-gray-900">
              Memberi makan kucing
            </label>
          </div>
          <div className="flex items-center">
            <Checkbox
              id="c2"
              checked={checkedItems[1]}
              onCheckedChange={(value) =>
                setCheckedItems([checkedItems[0], value])
              }
            />
            <label htmlFor="c2" className="ml-2 text-sm text-gray-900">
              Membersihkan kamar
            </label>
          </div>
        </div>
      </div>
      <h2 className="mt-8 text-xl font-bold">Todo Card</h2>
      <div className="mt-4">
        <TodoCard completed={completedOptions[0].value} />
      </div>
      <h2 className="mt-8 text-xl font-bold">Empty Todos State</h2>
      <div className="mt-4">
        <EmptyTodos />
      </div>
      <div className="pb-12" />
    </Layout>
  );
}
