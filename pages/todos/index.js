import { Button } from "@/components/Button";
import { Layout } from "@/components/Layout";
import { CircularLoading } from "@/components/Loading";
import { CreateTodoModal } from "@/components/Modal/CreateTodoModal";
import { EmptyTodos, TodoCard } from "@/components/Todo";
import { useState, useEffect } from "react";
import { FiPlus } from "react-icons/fi";
import { useGetUser } from "@/modules/user/hook";
import { useRouter } from "next/router";
import { useGetTodos } from "@/modules/todo/hook";

export default function TodoList() {
  const { data: user } = useGetUser();
  const dataHook = useGetTodos();
  const router = useRouter();
  let [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    if (user === null) router.replace("/signin");
  }, [user]);

  return (
    <Layout
      title="Todos"
      breadcrumbs={[{ title: "Todos", href: "/todos", isCurrent: true }]}
    >
      <h2 className="mb-8 text-xl italic text-indigo-500">
        Hi {user?.username}, welcome back!
      </h2>
      <h1 className="text-2xl font-bold">🖊️ Manage your todos</h1>
      <div className="pb-12 mt-6">
        {dataHook.status === "loading" ? (
          <div className="flex justify-center">
            <CircularLoading />
          </div>
        ) : dataHook.status === "error" ? (
          <span>Oops! Something went wrong.</span>
        ) : dataHook.data.response.length === 0 ? (
          <EmptyTodos onOpenModal={() => setIsOpen(true)} />
        ) : (
          <>
            <div className="flex justify-end">
              <Button onClick={() => setIsOpen(true)}>
                <FiPlus className="mr-2 text-lg" />
                Add todo
              </Button>
            </div>
            <div className="flex flex-col mt-4 space-y-4">
              {dataHook.data.response.map(({ todos: todo, tag }) => (
                <TodoCard
                  key={todo._id}
                  id={todo._id}
                  title={todo.todo}
                  subTodos={todo.items}
                  tags={tag}
                  status={todo.status}
                  completed={todo.completed}
                  description={todo.description}
                  image={todo.image}
                />
              ))}
            </div>
          </>
        )}
      </div>
      <CreateTodoModal isOpen={isOpen} onClose={() => setIsOpen(false)} />
    </Layout>
  );
}
