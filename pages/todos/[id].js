import { Button } from "@/components/Button";
import { Checkbox } from "@/components/Checkbox";
import { Layout } from "@/components/Layout";
import { Tag } from "@/components/Tag";
import Image from "next/image";
import { useRouter } from "next/router";
import { useEffect, useRef, useState } from "react";
import { FiAlertCircle, FiEdit2, FiPlus, FiUpload } from "react-icons/fi";
import { Select } from "@/components/Select";
import { completedOptions } from "utils/const";
import {
  EditTodoDescriptionModal,
  EditTodoModal,
  CreateSubTodoModal,
  EditSubTodoModal,
  DeleteModal,
} from "@/components/Modal";
import {
  useDeleteSubTodo,
  useDeleteTodo,
  useDeleteTodoBanner,
  useGetTodo,
  useInvalidateTodos,
  useUpdateCheckedSubTodo,
  useUpdateCheckedTodo,
  useUpdateCompletedTodo,
  useUpdateTodo,
} from "@/modules/todo/hook";
import { CircularLoading } from "@/components/Loading";
import { useGetUser } from "@/modules/user/hook";
import toast from "react-hot-toast";
import { imageToUrl } from "@/utils/helpers";

export default function Todo() {
  const router = useRouter();
  const { data: user } = useGetUser();

  useEffect(() => {
    if (user === null) router.replace("/signin");
  }, [user]);

  const { id } = router.query;
  const dataHook = useGetTodo(id);
  const invalidateQuery = useInvalidateTodos();
  const { response: todo } = dataHook.data || {};
  const selectedCompletedStatus = completedOptions.find(
    (option) => todo?.completed === option.value
  );
  const isBanner = !!todo?.image.data.data.length;

  const updateCompletedTodoMutation = useUpdateCompletedTodo();
  const updateCheckedTodoMutation = useUpdateCheckedTodo();
  const updateCheckedSubTodoMutation = useUpdateCheckedSubTodo();
  const updateTodoMutation = useUpdateTodo();
  const deleteTodoMutation = useDeleteTodo();
  const deleteTodoBannerMutation = useDeleteTodoBanner();
  const deleteSubTodoMutation = useDeleteSubTodo();

  const [selectedSubTodoToRemove, setSelectedSubTodoToRemove] = useState(null);
  const [selectedSubTodoToEdit, setSelectedSubTodoToEdit] = useState(null);

  const [isEditDescriptionModalOpen, setIsEditDescriptionModalOpen] =
    useState(false);
  const [isEditTodoModalOpen, setIsEditTodoModalOpen] = useState(false);
  const [isCreateSubTodoModalOpen, setIsCreateSubTodoModalOpen] =
    useState(false);
  const [isEditSubTodoModalOpen, setIsEditSubTodoModalOpen] = useState(false);
  const [isDeleteSubTodoModalOpen, setIsDeleteSubTodoModalOpen] =
    useState(false);
  const [isDeleteTodoModalOpen, setIsDeleteTodoModalOpen] = useState(false);

  const inputFileRef = useRef(null);
  const onImageChange = (e) => {
    if (e.target.files && e.target.files.length > 0) {
      const imageFile = e.target.files[0];
      const formData = new FormData();
      formData.append("image", imageFile);
      updateTodoMutation.mutate(
        { id, data: formData },
        {
          onSuccess: () => {
            toast.success("Banner updated!");
            invalidateQuery();
          },
          onError: (err) => {
            console.log(err);
            toast.error(err.message);
          },
        }
      );
    }
  };

  const onUploadButtonClick = () => {
    inputFileRef.current.click();
  };

  const onRemoveBanner = () => {
    deleteTodoBannerMutation.mutate(todo._id, {
      onSuccess: () => {
        toast.success("Banner removed!");
        invalidateQuery();
      },
      onError: (err) => {
        console.log(err);
        toast.error(err.message);
      },
    });
  };

  const onCompletedStatusChange = (selectedOption) => {
    updateCompletedTodoMutation.mutate(
      {
        id: todo._id,
        data: {
          completed: selectedOption.value,
        },
      },
      {
        onSuccess: () => {
          toast.success("Status updated!");
          invalidateQuery();
        },
        onError: (err) => {
          console.log(err);
          toast.error(err.message);
        },
      }
    );
  };

  const onTodoCheckedChange = (isChecked) => {
    updateCheckedTodoMutation.mutate(
      {
        id: todo._id,
        data: {
          status: isChecked ? "checked" : "uncheck",
        },
      },
      {
        onSuccess: () => {
          toast.success("Todo updated!");
          invalidateQuery();
        },
        onError: (err) => {
          console.log(err);
          toast.error(err.message);
        },
      }
    );
  };

  const onDeleteSubTodo = () => {
    deleteSubTodoMutation.mutate(
      {
        idTodo: todo._id,
        idItemTodo: selectedSubTodoToRemove._id,
      },
      {
        onSuccess: () => {
          toast.success("Sub todo removed!");
          invalidateQuery();
        },
        onError: (err) => {
          console.log(err);
          toast.error(err.message);
        },
      }
    );
  };

  const onDeleteTodo = () => {
    deleteTodoMutation.mutate(todo._id, {
      onSuccess: () => {
        router.push("/todos");
        toast.success("Todo deleted!");
      },
      onError: (err) => {
        console.log(err);
        toast.error(err.message);
      },
    });
  };

  return (
    <Layout
      title="Todo"
      breadcrumbs={[
        { title: "Todos", href: "/todos" },
        { title: todo?.todo || "...", href: `/todos/${id}`, isCurrent: true },
      ]}
    >
      <div className="flex flex-col pb-24 mt-2">
        {dataHook.status === "loading" ? (
          <div className="flex justify-center">
            <CircularLoading />
          </div>
        ) : !todo ? (
          <span>Oops! Todo not found.</span>
        ) : (
          <div className="flex flex-col space-y-10">
            <div>
              <p className="mb-2 text-indigo-600">Banner</p>
              <Image
                src={
                  !!isBanner
                    ? imageToUrl(todo.image)
                    : "/images/imagePlaceholder.png"
                }
                alt="thumbnail"
                width={600}
                height={200}
                objectFit="cover"
                objectPosition="center"
                className="mt-6 rounded-md"
              />
              <div className="flex mt-2 space-x-2">
                <div className="w-full">
                  <input
                    type="file"
                    ref={inputFileRef}
                    className="hidden"
                    onChange={onImageChange}
                  />
                  <Button
                    size="sm"
                    w="full"
                    onClick={onUploadButtonClick}
                    disabled={updateTodoMutation.isLoading}
                  >
                    <FiUpload className="mr-2" />
                    {updateTodoMutation.isLoading ? "Uploading..." : "Change"}
                  </Button>
                </div>
                <Button
                  size="sm"
                  variant="secondary"
                  w="full"
                  onClick={onRemoveBanner}
                  disabled={!isBanner || updateTodoMutation.isLoading}
                >
                  Remove
                </Button>
              </div>
            </div>
            <div>
              <p className="mb-2 text-indigo-600">Description</p>
              <p>
                {todo.description || (
                  <span className="text-gray-500">None</span>
                )}
              </p>
              <div className="flex justify-end mt-2">
                <Button
                  size="sm"
                  onClick={() => setIsEditDescriptionModalOpen(true)}
                >
                  <FiEdit2 className="mr-2" />
                  Edit
                </Button>
                <EditTodoDescriptionModal
                  id={todo._id}
                  isOpen={isEditDescriptionModalOpen}
                  onClose={() => setIsEditDescriptionModalOpen(false)}
                  defaultValues={{ description: todo.description }}
                />
              </div>
            </div>
            <div>
              <p className="mb-2 text-indigo-600">Status</p>
              <div className="mt-4">
                <Select
                  value={selectedCompletedStatus}
                  options={completedOptions}
                  onChange={onCompletedStatusChange}
                />
              </div>
            </div>
            <div>
              <p className="mb-4 text-indigo-600">Todos</p>
              <div className="flex items-center space-x-6">
                <div className="flex items-center">
                  <Checkbox
                    id={todo._id}
                    checked={todo.status === "checked"}
                    onCheckedChange={onTodoCheckedChange}
                  />
                  <label
                    htmlFor={todo._id}
                    className="ml-2 text-sm text-gray-900"
                  >
                    {todo.todo}
                  </label>
                </div>
                <div className="flex space-x-4">
                  <Button
                    size="text"
                    variant="text"
                    onClick={() => setIsEditTodoModalOpen(true)}
                  >
                    Edit
                  </Button>
                  <EditTodoModal
                    id={todo._id}
                    defaultValues={todo}
                    isOpen={isEditTodoModalOpen}
                    onClose={() => setIsEditTodoModalOpen(false)}
                  />
                </div>
              </div>
              <div className="flex flex-col pl-8 mt-4 space-y-4">
                {todo.items.map((subTodo) => {
                  const onSubTodoCheckedChange = (isChecked) => {
                    updateCheckedSubTodoMutation.mutate(
                      {
                        idTodo: todo._id,
                        idItemTodo: subTodo._id,
                        status: isChecked ? "checked" : "uncheck",
                      },
                      {
                        onSuccess: () => {
                          toast.success("Sub todo updated!");
                          invalidateQuery();
                        },
                        onError: (err) => {
                          console.log(err);
                          toast.error(err.message);
                        },
                      }
                    );
                  };

                  return (
                    <div key={subTodo._id}>
                      <div className="flex items-center space-x-6">
                        <div className="flex items-center">
                          <Checkbox
                            id={subTodo._id}
                            checked={subTodo.statusItem === "checked"}
                            onCheckedChange={onSubTodoCheckedChange}
                          />
                          <label
                            htmlFor={subTodo._id}
                            className="ml-2 text-sm text-gray-900"
                          >
                            {subTodo.item}
                          </label>
                        </div>
                        <div className="flex space-x-4">
                          <Button
                            size="text"
                            variant="text"
                            onClick={() => {
                              setSelectedSubTodoToEdit(subTodo);
                              setIsEditSubTodoModalOpen(true);
                            }}
                          >
                            Edit
                          </Button>
                          <Button
                            size="text"
                            variant="text-danger"
                            onClick={() => {
                              setSelectedSubTodoToRemove(subTodo);
                              setIsDeleteSubTodoModalOpen(true);
                            }}
                          >
                            Remove
                          </Button>
                        </div>
                      </div>
                      {!!subTodo.tag.length && (
                        <div className="flex flex-wrap mt-2 space-x-2">
                          {subTodo.tag.map((label, idx) => (
                            <Tag key={idx}>{label}</Tag>
                          ))}
                        </div>
                      )}
                    </div>
                  );
                })}
                <Button
                  size="sm"
                  onClick={() => setIsCreateSubTodoModalOpen(true)}
                >
                  <FiPlus className="mr-2" />
                  Add sub todo
                </Button>
                <CreateSubTodoModal
                  id={todo._id}
                  isOpen={isCreateSubTodoModalOpen}
                  onClose={() => setIsCreateSubTodoModalOpen(false)}
                />
              </div>
            </div>
            <div className="pt-6">
              <Button
                variant="danger"
                w="full"
                onClick={() => setIsDeleteTodoModalOpen(true)}
              >
                <FiAlertCircle className="mr-2" />
                Delete Todo
              </Button>
              <DeleteModal
                title="Delete Todo"
                isOpen={isDeleteTodoModalOpen}
                onClose={() => setIsDeleteTodoModalOpen(false)}
                onDelete={onDeleteTodo}
              />
            </div>
          </div>
        )}
      </div>

      {selectedSubTodoToEdit && (
        <EditSubTodoModal
          todoId={todo._id}
          subTodoId={selectedSubTodoToEdit._id}
          defaultValues={{
            itemTodo: selectedSubTodoToEdit.item,
            tag: selectedSubTodoToEdit.tag.join(" "),
          }}
          isOpen={isEditSubTodoModalOpen}
          onClose={() => setIsEditSubTodoModalOpen(false)}
        />
      )}
      {selectedSubTodoToRemove && (
        <DeleteModal
          title="Delete Sub Todo"
          isOpen={isDeleteSubTodoModalOpen}
          onClose={() => setIsDeleteSubTodoModalOpen(false)}
          onDelete={onDeleteSubTodo}
          isLoading={deleteSubTodoMutation.isLoading}
        />
      )}
    </Layout>
  );
}
