import { Button } from "@/components/Button";
import { Layout } from "@/components/Layout";
import { useGetUser } from "@/modules/user/hook";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { useForm } from "react-hook-form";
import toast from "react-hot-toast";
import { fetcher } from "utils/fetcher";

export default function Register() {
  const router = useRouter();
  const {
    register,
    handleSubmit,
    formState: { isSubmitting },
  } = useForm();
  const { data: user } = useGetUser();

  useEffect(() => {
    if (user) router.replace("/todos");
  }, [user]);

  const onRegister = async (data) => {
    try {
      if (data.password !== data.passwordConfirmation) {
        throw new Error("Password confirmation does not match");
      }

      const res = await fetcher.post("/register", data);

      if (!res.data.success) throw new Error(res.data.info);

      router.push("/signin");
      toast.success("Register success!");
    } catch (err) {
      console.log(err);
      toast.error(err.message);
    }
  };

  return (
    <Layout title="Register">
      <div className="flex items-center justify-center min-h-full py-6">
        <div className="w-full max-w-md space-y-8">
          <div>
            <h2 className="mt-6 text-3xl font-extrabold text-center text-gray-900">
              Register
            </h2>
            <p className="mt-2 text-sm text-center text-gray-600">
              Or
              <Link href="/signin">
                <a className="font-medium text-indigo-600 hover:text-indigo-500">
                  {" "}
                  sign in
                </a>
              </Link>
            </p>
          </div>
          <form className="mt-8 space-y-6" onSubmit={handleSubmit(onRegister)}>
            <div className="-space-y-px rounded-md shadow-sm">
              <div>
                <label className="sr-only">Username</label>
                <input
                  name="username"
                  type="text"
                  required
                  className="relative block w-full px-3 py-2 text-gray-900 placeholder-gray-500 border border-gray-300 rounded-none appearance-none rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                  placeholder="Username"
                  {...register("username")}
                />
              </div>
              <div>
                <label className="sr-only">Password</label>
                <input
                  name="password"
                  type="password"
                  required
                  className="relative block w-full px-3 py-2 text-gray-900 placeholder-gray-500 border border-gray-300 rounded-none appearance-none focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                  placeholder="Password"
                  {...register("password")}
                />
              </div>
              <div>
                <label className="sr-only">Confirm Password</label>
                <input
                  name="confirmPassword"
                  type="password"
                  required
                  className="relative block w-full px-3 py-2 text-gray-900 placeholder-gray-500 border border-gray-300 rounded-none appearance-none rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                  placeholder="Confirm password"
                  {...register("passwordConfirmation")}
                />
              </div>
            </div>
            <Button w="full" type="submit" disabled={isSubmitting}>
              Register
            </Button>
          </form>
        </div>
      </div>
    </Layout>
  );
}
