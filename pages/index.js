import { Button } from "@/components/Button";
import { Layout } from "@/components/Layout";
import Image from "next/image";

export default function Home() {
  return (
    <Layout title="Home">
      <h1 className="mt-4 mt-12 text-2xl font-bold text-center">
        Welcome to To Do App
      </h1>
      <div className="flex justify-center mt-4 space-x-2">
        <Button href="/register">Register</Button>
        <Button variant="secondary" href="/signin">
          Sign in
        </Button>
      </div>
      <div className="flex justify-center mt-12">
        <Image
          src="/images/brad.gif"
          alt="thumbnail"
          width={300}
          height={300}
          objectFit="cover"
          objectPosition="center"
          className="rounded-md"
        />
      </div>
    </Layout>
  );
}
