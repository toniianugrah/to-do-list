import { logout } from "@/modules/user/api";
import { useGetUser, useInvalidateUser } from "@/modules/user/hook";
import Head from "next/head";
import Link from "next/link";
import toast from "react-hot-toast";
import { Breadcrumb } from "../Breadcrumb";

export const Layout = ({ title, children, breadcrumbs = [] }) => {
  const { data: user } = useGetUser();
  const invalidateUser = useInvalidateUser();

  const onLogout = async () => {
    await logout();
    invalidateUser();
    toast.success("Log out success!");
  };

  return (
    <>
      <Head>
        <title>{title}</title>
        <meta name="description" content="To do list app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className="max-w-lg min-h-screen px-4 mx-auto">
        <nav className="flex items-center justify-between py-6">
          <p className="text-lg font-bold text-indigo-600">
            <Link href="/">TodoApp</Link>
          </p>
          <ul className="flex space-x-4">
            {user ? (
              <li>
                <button
                  className="underline hover:text-indigo-700"
                  onClick={onLogout}
                >
                  Log out
                </button>
              </li>
            ) : (
              <>
                <li>
                  <Link href="/register">
                    <a className="underline hover:text-indigo-700">Register</a>
                  </Link>
                </li>
                <li>
                  <Link href="/signin">
                    <a className="underline hover:text-indigo-700">Sign in</a>
                  </Link>
                </li>
              </>
            )}
          </ul>
        </nav>
        {!!breadcrumbs.length && <Breadcrumb breadcrumbs={breadcrumbs} />}
        {children}
      </div>
    </>
  );
};
