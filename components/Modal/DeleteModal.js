import { useRef } from "react";
import { Button } from "../Button";
import { Modal } from "./Modal";

export const DeleteModal = ({
  isOpen,
  onClose,
  title,
  onDelete,
  description = "Are you sure you want to delete this item?",
  deleteLabel = "Delete",
  isLoading = false,
}) => {
  const cancelButtonRef = useRef(null);

  return (
    <Modal
      initialFocusRef={cancelButtonRef}
      isOpen={isOpen}
      onClose={onClose}
      title={title}
      body={<p className="-mt-2">{description}</p>}
      footer={
        <>
          <Button
            type="button"
            variant="danger"
            onClick={() => {
              onDelete();
              onClose();
            }}
            disabled={isLoading}
          >
            {deleteLabel}
          </Button>
          <Button
            ref={cancelButtonRef}
            type="button"
            variant="neutral"
            onClick={onClose}
            disabled={isLoading}
          >
            Cancel
          </Button>
        </>
      }
    />
  );
};
