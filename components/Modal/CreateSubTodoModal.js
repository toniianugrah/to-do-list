import { useCreateSubTodo, useInvalidateTodos } from "@/modules/todo/hook";
import { useForm } from "react-hook-form";
import toast from "react-hot-toast";
import { Button } from "../Button";
import { Input } from "../Input";
import { Modal } from "./Modal";

export const CreateSubTodoModal = ({ id, isOpen, onClose }) => {
  const { register, handleSubmit } = useForm();
  const mutation = useCreateSubTodo();
  const invalidateQuery = useInvalidateTodos();

  const onCreate = (data) => {
    mutation.mutate(
      { ...data, idTodo: id, tag: "" },
      {
        onSuccess: () => {
          toast.success("Sub todo created!");
          onClose();
          invalidateQuery();
        },
        onError: (err) => {
          console.log(err);
          toast.error(err.message);
        },
      }
    );
  };

  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
      title="Create Sub Todo"
      body={
        <form onSubmit={handleSubmit(onCreate)} id="add-sub-todo-form">
          <label>Title</label>
          <Input {...register("itemTodo")} />
        </form>
      }
      footer={
        <>
          <Button
            type="submit"
            form="add-sub-todo-form"
            disabled={mutation.isLoading}
          >
            Save
          </Button>
          <Button
            type="button"
            variant="secondary"
            onClick={onClose}
            disabled={mutation.isLoading}
          >
            Cancel
          </Button>
        </>
      }
    />
  );
};
