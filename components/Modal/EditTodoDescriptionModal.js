import { useInvalidateTodos, useUpdateTodo } from "@/modules/todo/hook";
import { useForm } from "react-hook-form";
import toast from "react-hot-toast";
import { Button } from "../Button";
import { Input } from "../Input";
import { Modal } from "./Modal";

export const EditTodoDescriptionModal = ({
  id,
  defaultValues,
  isOpen,
  onClose,
}) => {
  const { register, handleSubmit } = useForm({ defaultValues });
  const mutation = useUpdateTodo();
  const invalidateQuery = useInvalidateTodos();

  const onUpdate = (data) => {
    mutation.mutate(
      { id, data },
      {
        onSuccess: () => {
          toast.success("Description updated!");
          onClose();
          invalidateQuery();
        },
        onError: (err) => {
          console.log(err);
          toast.error(err.message);
        },
      }
    );
  };

  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
      title="Edit Description"
      body={
        <form onSubmit={handleSubmit(onUpdate)} id="edit-todo-description-form">
          <label>Description</label>
          <Input {...register("description")} />
        </form>
      }
      footer={
        <>
          <Button
            type="submit"
            form="edit-todo-description-form"
            disabled={mutation.isLoading}
          >
            Save
          </Button>
          <Button
            type="button"
            variant="secondary"
            onClick={onClose}
            disabled={mutation.isLoading}
          >
            Cancel
          </Button>
        </>
      }
    />
  );
};
