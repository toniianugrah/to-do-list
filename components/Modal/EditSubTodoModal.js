import { useInvalidateTodos, useUpdateSubTodo } from "@/modules/todo/hook";
import { useEffect } from "react";
import { useForm } from "react-hook-form";
import toast from "react-hot-toast";
import { Button } from "../Button";
import { Input } from "../Input";
import { Modal } from "./Modal";

export const EditSubTodoModal = ({
  todoId,
  subTodoId,
  defaultValues,
  isOpen,
  onClose,
}) => {
  const { register, handleSubmit, reset } = useForm();
  const mutation = useUpdateSubTodo();
  const invalidateQuery = useInvalidateTodos();

  useEffect(() => {
    reset(defaultValues);
  }, [defaultValues]);

  const onUpdate = (data) => {
    mutation.mutate(
      { id: todoId, data: { ...data, idItemTodo: subTodoId } },
      {
        onSuccess: () => {
          toast.success("Sub todo updated!");
          onClose();
          invalidateQuery();
        },
        onError: (err) => {
          console.log(err);
          toast.error(err.message);
        },
      }
    );
  };

  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
      title="Edit Sub Todo"
      body={
        <form onSubmit={handleSubmit(onUpdate)} id="edit-sub-todo-form">
          <div>
            <label>Title</label>
            <Input {...register("itemTodo")} />
          </div>
          <div className="mt-4">
            <label>Tags</label>
            <Input {...register("tag")} />
            <p className="mt-2 text-sm text-gray-500">
              Format: #urgent #fun #important
            </p>
          </div>
        </form>
      }
      footer={
        <>
          <Button
            type="submit"
            form="edit-sub-todo-form"
            disabled={mutation.isLoading}
          >
            Save
          </Button>
          <Button
            type="button"
            variant="secondary"
            onClick={onClose}
            disabled={mutation.isLoading}
          >
            Cancel
          </Button>
        </>
      }
    />
  );
};
