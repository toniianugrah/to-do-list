import { useInvalidateTodos, useUpdateTodo } from "@/modules/todo/hook";
import { useForm } from "react-hook-form";
import toast from "react-hot-toast";
import { Button } from "../Button";
import { Input } from "../Input";
import { Modal } from "./Modal";

export const EditTodoModal = ({ id, defaultValues, isOpen, onClose }) => {
  const { register, handleSubmit } = useForm({ defaultValues });
  const mutation = useUpdateTodo();
  const invalidateQuery = useInvalidateTodos();

  const onUpdate = (data) => {
    mutation.mutate(
      { id, data },
      {
        onSuccess: () => {
          toast.success("Todo updated!");
          onClose();
          invalidateQuery();
        },
        onError: (err) => {
          console.log(err);
          toast.error(err.message);
        },
      }
    );
  };

  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
      title="Edit Todo"
      body={
        <form onSubmit={handleSubmit(onUpdate)} id="edit-todo-form">
          <label>Title</label>
          <Input {...register("todo")} />
        </form>
      }
      footer={
        <>
          <Button
            type="submit"
            form="edit-todo-form"
            disabled={mutation.isLoading}
          >
            Save
          </Button>
          <Button
            type="button"
            variant="secondary"
            onClick={onClose}
            disabled={mutation.isLoading}
          >
            Cancel
          </Button>
        </>
      }
    />
  );
};
