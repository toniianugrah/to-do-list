import { useCreateTodo, useInvalidateTodos } from "@/modules/todo/hook";
import { useForm } from "react-hook-form";
import toast from "react-hot-toast";
import { Button } from "../Button";
import { Input } from "../Input";
import { Modal } from "./Modal";

export const CreateTodoModal = ({ isOpen, onClose }) => {
  const { register, handleSubmit, watch } = useForm();
  const mutation = useCreateTodo();
  const invalidateQuery = useInvalidateTodos();

  const onCreate = (data) => {
    mutation.mutate(data, {
      onSuccess: () => {
        toast.success("Todo created!");
        onClose();
        invalidateQuery();
      },
      onError: (err) => {
        console.log(err);
        toast.error(err.message);
      },
    });
  };

  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
      title="Create Todo"
      body={
        <form
          onSubmit={handleSubmit(onCreate)}
          id="add-todo-form"
          className="pb-4"
        >
          <div>
            <label>Title</label>
            <Input {...register("todo")} />
          </div>
        </form>
      }
      footer={
        <>
          <Button
            type="submit"
            form="add-todo-form"
            disabled={mutation.isLoading}
          >
            Save
          </Button>
          <Button
            type="button"
            variant="secondary"
            onClick={onClose}
            disabled={mutation.isLoading}
          >
            Cancel
          </Button>
        </>
      }
    />
  );
};
