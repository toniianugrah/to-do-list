import * as RadixCheckbox from "@radix-ui/react-checkbox";
import clsx from "clsx";
import { FiCheck, FiMinus } from "react-icons/fi";

export const Checkbox = ({
  id,
  checked = false,
  onCheckedChange,
  disabled,
}) => {
  return (
    <RadixCheckbox.Root
      id={id}
      checked={checked}
      onCheckedChange={onCheckedChange}
      className={clsx(
        "w-6 h-6 flex justify-center items-center text-white border-[#433FFF] border overflow-hidden rounded focus:ring-[#433FFF] focus:ring-1",
        checked && "bg-[#433FFF]"
      )}
      disabled={disabled}
    >
      <RadixCheckbox.Indicator className="text-lg text-white">
        {checked === "indeterminate" && <FiMinus />}
        {checked === true && <FiCheck />}
      </RadixCheckbox.Indicator>
    </RadixCheckbox.Root>
  );
};
