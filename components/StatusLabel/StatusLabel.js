import clsx from "clsx";

export const StatusLabel = ({ variant = "inprogress", children }) => {
  const variantInProgress = clsx("bg-[#adf2f673]");
  const variantBlocked = clsx("bg-[#f6adb573]");
  const variantDone = clsx("bg-[#7879F1]");

  const style = clsx(
    "px-2 py-1 rounded-lg max-w-max",
    variant === "inprogress" && variantInProgress,
    variant === "blocked" && variantBlocked,
    variant === "done" && variantDone
  );

  return <div className={style}>{children}</div>;
};
