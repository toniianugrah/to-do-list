import clsx from "clsx";
import Link from "next/link";

export const Breadcrumb = ({ breadcrumbs }) => {
  return (
    <nav aria-label="Breadcrumb" className="w-full pt-4 pb-8">
      <ol className="flex items-center justify-start space-x-2">
        {breadcrumbs.map(({ isCurrent, href, title }, i) => {
          return (
            <li key={href} className="flex items-center space-x-2">
              {i !== 0 && isCurrent && <div aria-hidden>&gt;</div>}
              <Link href={href}>
                <a
                  className={clsx(
                    "text-slate-600 hover:text-slate-700 hover:underline",
                    isCurrent && "underline"
                  )}
                  aria-current={isCurrent ? "page" : false}
                >
                  {title}
                </a>
              </Link>
            </li>
          );
        })}
      </ol>
    </nav>
  );
};
