export const Tag = ({ children }) => {
  return (
    <div className="px-2 py-1 text-sm border border-gray-400 rounded-md">
      {children}
    </div>
  );
};
