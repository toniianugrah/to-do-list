import {
  useInvalidateTodos,
  useUpdateCheckedSubTodo,
  useUpdateCheckedTodo,
} from "@/modules/todo/hook";
import { completedOptions } from "@/utils/const";
import { imageToUrl } from "@/utils/helpers";
import Image from "next/image";
import toast from "react-hot-toast";
import { Button } from "../Button";
import { Checkbox } from "../Checkbox";
import { StatusLabel } from "../StatusLabel";
import { Tag } from "../Tag";

export const TodoCard = ({
  id,
  title,
  description,
  status,
  completed,
  image,
  subTodos = [],
  tags = [],
}) => {
  const invalidateQuery = useInvalidateTodos();
  const updateCheckedTodoMutation = useUpdateCheckedTodo();
  const updateCheckedSubTodoMutation = useUpdateCheckedSubTodo();
  const selectedCompleted = completedOptions.find(
    (option) => option.value === completed
  );
  const isBanner = !!image?.data.data.length;

  const onTodoCheckedChange = (isChecked) => {
    updateCheckedTodoMutation.mutate(
      {
        id: id,
        data: {
          status: isChecked ? "checked" : "uncheck",
        },
      },
      {
        onSuccess: () => {
          toast.success("Todo updated!");
          invalidateQuery();
        },
        onError: (err) => {
          console.log(err);
          toast.error(err.message);
        },
      }
    );
  };

  return (
    <div className="p-4 text-sm border border-gray-300 rounded-md">
      <div className="flex items-center justify-between space-x-4">
        <div className="flex items-center">
          <Checkbox
            onCheckedChange={onTodoCheckedChange}
            checked={status === "checked"}
            id={id}
          />
          <label htmlFor={id} className="ml-2 text-sm text-gray-900">
            {title}
          </label>
        </div>
        <StatusLabel variant={selectedCompleted.variant}>
          {selectedCompleted.label}
        </StatusLabel>
      </div>
      <div className="flex flex-col pl-8 mt-4 space-y-4">
        {isBanner && (
          <Image
            src={imageToUrl(image)}
            alt="thumbnail"
            width={600}
            height={200}
            objectFit="cover"
            objectPosition="center"
            className="rounded-md"
          />
        )}
        {description && (
          <div className="rounded-md p-4 bg-[#F0F0F8]">
            <p>{description}</p>
          </div>
        )}
        <div className="flex flex-col space-y-4">
          {subTodos.map((subTodo) => {
            const onSubTodoCheckedChange = (isChecked) => {
              updateCheckedSubTodoMutation.mutate(
                {
                  idTodo: id,
                  idItemTodo: subTodo._id,
                  status: isChecked ? "checked" : "uncheck",
                },
                {
                  onSuccess: () => {
                    toast.success("Sub todo updated!");
                    invalidateQuery();
                  },
                  onError: (err) => {
                    console.log(err);
                    toast.error(err.message);
                  },
                }
              );
            };

            return (
              <div className="flex items-center" key={subTodo._id}>
                <Checkbox
                  id={subTodo._id}
                  checked={subTodo.statusItem === "checked"}
                  onCheckedChange={onSubTodoCheckedChange}
                />
                <label
                  htmlFor={subTodo._id}
                  className="ml-2 text-sm text-gray-900"
                >
                  {subTodo.item}
                </label>
              </div>
            );
          })}
        </div>
      </div>
      {!!tags.length && (
        <div className="flex mt-6 space-x-2">
          {tags.map((label, idx) => (
            <Tag key={idx}>{label}</Tag>
          ))}
        </div>
      )}
      <div className="flex justify-end mt-6 space-x-2">
        <Button w="full" variant="secondary" href={`/todos/${id}`}>
          Detail
        </Button>
      </div>
    </div>
  );
};
