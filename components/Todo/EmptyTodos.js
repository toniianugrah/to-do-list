import { FiPlus } from "react-icons/fi";
import { Button } from "../Button";

export const EmptyTodos = ({ onOpenModal }) => {
  return (
    <div className="flex flex-col items-center p-12 px-24 text-sm border border-gray-300 rounded-md">
      <p className="text-xl font-bold text-center">Create a Todo</p>
      <p className="mt-2 mb-6 text-center text-gray-500">
        A todo will help you to manage your task and increase your productivity.
      </p>
      <Button onClick={onOpenModal}>
        <FiPlus className="mr-2 text-lg" /> Create a todo
      </Button>
    </div>
  );
};
