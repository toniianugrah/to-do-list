import { forwardRef } from "react";

export const Input = forwardRef(({ ...rest }, ref) => {
  return (
    <input
      ref={ref}
      type="text"
      className="block w-full mt-2 border-gray-300 rounded-md shadow-sm focus:border-indigo-400 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
      {...rest}
    />
  );
});

Input.displayName = "Input";
