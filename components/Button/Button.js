import clsx from "clsx";
import Link from "next/link";
import { forwardRef } from "react";

export const Button = forwardRef(
  (
    {
      variant = "primary",
      children,
      href,
      w = "max",
      size = "md",
      type = "button",
      ...rest
    },
    ref
  ) => {
    const variantPrimary =
      "text-white bg-indigo-600 font-medium focus:ring-indigo-500 hover:bg-indigo-700 border-transparent";
    const variantSecondary =
      "text-indigo-700  font-medium focus:ring-indigo-500 hover:bg-indigo-100 border-indigo-400";
    const variantDanger =
      "text-white bg-red-600 font-medium focus:ring-red-500 hover:bg-red-700 border-transparent";
    const variantNeutral =
      "text-gray-800 font-medium hover:bg-gray-50 border-gray-400";
    const variantText =
      "text-indigo-600 hover:text-indigo-800 focus:ring-indigo-500 hover:underline border-transparent";
    const variantTextDanger =
      "text-red-600 hover:text-red-800 hover:underline border-transparent";

    const text = clsx("text-sm");
    const sm = clsx("px-3 py-1 text-sm focus:ring-2");
    const md = clsx("px-4 py-2 text-base focus:ring-2");

    const style = clsx(
      "relative flex items-center justify-center w-full border rounded-md focus:outline-none focus:ring-offset-2",
      "disabled:bg-gray-100 disabled:border-gray-400 disabled:text-gray-500",
      variant === "primary" && variantPrimary,
      variant === "secondary" && variantSecondary,
      variant === "danger" && variantDanger,
      variant === "neutral" && variantNeutral,
      variant === "text" && variantText,
      variant === "text-danger" && variantTextDanger,
      w === "max" && "max-w-max",
      size === "sm" && sm,
      size === "md" && md,
      (size = "text" && text)
    );

    if (href) {
      return (
        <Link href={href}>
          <a className={style}>{children}</a>
        </Link>
      );
    }

    return (
      <button ref={ref} type={type} className={style} {...rest}>
        {children}
      </button>
    );
  }
);

Button.displayName = "Button";
