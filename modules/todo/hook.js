import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import {
  getTodos,
  createTodo,
  getTodo,
  updateCompletedTodo,
  updateCheckedTodo,
  updateTodo,
  updateSubTodo,
  createSubTodo,
  deleteSubTodo,
  updateCheckedSubTodo,
  deleteTodo,
  deleteTodoBanner,
} from "./api";

export const useGetTodos = () => {
  return useQuery(["todos"], getTodos);
};

export const useGetTodo = (id) => {
  return useQuery(["todos", id], () => getTodo(id));
};

export const useCreateTodo = () => {
  return useMutation(createTodo);
};

export const useCreateSubTodo = () => {
  return useMutation(createSubTodo);
};

export const useUpdateTodo = () => {
  return useMutation(({ id, data }) => updateTodo(id, data));
};

export const useUpdateSubTodo = () => {
  return useMutation(({ id, data }) => updateSubTodo(id, data));
};

export const useUpdateCompletedTodo = () => {
  return useMutation(({ id, data }) => updateCompletedTodo(id, data));
};

export const useUpdateCheckedTodo = () => {
  return useMutation(({ id, data }) => updateCheckedTodo(id, data));
};

export const useUpdateCheckedSubTodo = () => {
  return useMutation(updateCheckedSubTodo);
};

export const useDeleteSubTodo = () => {
  return useMutation(deleteSubTodo);
};

export const useDeleteTodo = () => {
  return useMutation(deleteTodo);
};

export const useDeleteTodoBanner = () => {
  return useMutation(deleteTodoBanner);
};

export const useInvalidateTodos = () => {
  const queryClient = useQueryClient();
  return () => queryClient.invalidateQueries(["todos"]);
};
