import { fetcher } from "@/utils/fetcher";

export const getTodos = async () => {
  const res = await fetcher.get("/todo");
  return res.data;
};

export const getTodo = async (id) => {
  const res = await fetcher.get(`/todo/${id}`);
  return res.data;
};

export const createTodo = async (data) => {
  const res = await fetcher.post("/todo/add", data);
  return res.data;
};

export const updateTodo = async (id, data) => {
  const res = await fetcher.put(`/todo/edit/${id}`, data);
  return res.data;
};

export const updateSubTodo = async (id, data) => {
  const res = await fetcher.put(`/todo/item/edit`, { idTodo: id, ...data });
  return res.data;
};

export const updateCompletedTodo = async (id, data) => {
  const res = await fetcher.put(`/todo/editCompleted/${id}`, data);
  return res.data;
};

export const updateCheckedTodo = async (id, data) => {
  const res = await fetcher.put(`/todo/editStatus/${id}`, data);
  return res.data;
};

export const updateCheckedSubTodo = async (data) => {
  const res = await fetcher.put(`/todo/item/editStatus`, data);
  return res.data;
};

export const createSubTodo = async (data) => {
  const res = await fetcher.post("/todo/item/add", data);
  return res.data;
};

export const deleteSubTodo = async (data) => {
  const res = await fetcher.post("/todo/item/delete", data);
  return res.data;
};

export const deleteTodo = async (id) => {
  const res = await fetcher.delete(`/todo/delete/${id}`);
  return res.data;
};

export const deleteTodoBanner = async (id) => {
  const res = await fetcher.delete(`/todo/image/${id}`);
  return res.data;
};
