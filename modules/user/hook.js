import { useQuery, useQueryClient } from "@tanstack/react-query";
import { getUser } from "./api";

export const useGetUser = () => {
  const queryClient = useQueryClient();

  return useQuery(["user"], getUser, {
    retry: false,
    onError: () => {
      queryClient.setQueryData(["user"], null);
    },
  });
};

export const useInvalidateUser = () => {
  const queryClient = useQueryClient();
  return () => queryClient.invalidateQueries(["user"]);
};
