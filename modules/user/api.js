import { fetcher } from "@/utils/fetcher";

export const getUser = async () => {
  const res = await fetcher.get("/todo");
  return res.data;
};

export const login = async (loginCredentials) => {
  const res = await fetcher.post("/login", loginCredentials);
  return res.data;
};

export const logout = async () => {
  const res = await fetcher.get("/logout");
  return res.data;
};
